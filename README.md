# Setup Clang

* Download the binary from http://releases.llvm.org/download.html
* A few things to take note
    * PATH
    * STYLE
    * FALLBACK STYLE

* Style option can be found here
    * https://clang.llvm.org/docs/ClangFormatStyleOptions.html

* Sample setup on VS Code
~~~
    "C_Cpp.clang_format_path": "C:\\Program Files\\LLVM\\bin\\clang-format.exe",
    "C_Cpp.clang_format_style": "{IndentWidth: 4, ColumnLimit: 0, AlignConsecutiveAssignments: true, AlignConsecutiveDeclarations: true, AlignTrailingComments: true, BreakBeforeBraces: Allman, PointerAlignment: Right, AlignAfterOpenBracket: Align, AlignEscapedNewlines: Left, AlignOperands: true}",
    "C_Cpp.clang_format_fallbackStyle": "none",
~~~

# Jetbrains

* My settings for Android Studio, Intellij...
* Should be compatible with all JetBrains products

## Keymap
~~~
* CTRL + h/j/k/l        = vim style navigation
* CTRL + e/y            = vim style scroll up/down
* CTRL + d/u            = vim style page Down/Up

* CTRL + ALT + h        = Replace
* CTRL + ALT + k        = VCS Commit

* F5                    = Build
* CTRL + F5 	        = Build & Run
* SHIFT + F5	        = Stop

* F2 / F10              = Next
* F3 / F11              = Previous
* F4 / F12              = Definition

* SHIFT + F6	        = Smart Rename
* CTRL + n              = Generate Function
* ALT + ENTER           = Generate definition, quick fix for library inclusion, etc... (when words turn red)

* SHIFT + ENTER         = Enter a new line below cursor

* F1                    = Step over 
* SHIFT + F1            = Step into
* ALT + F1              = Step out of
~~~


# Linux

This is the `bash_aliases` file for all my bash settings

* Setup minimal ubuntu server OS + XFCE4
```
    Basic stuff
    -----------
    sudo apt install xfce4     (use `startx` to fire up GUI)
    sudo apt install chromium-browser
    sudo apt install gnome-terminal
    sudo apt install network-manager-gnome

    Windows Remote XRDP
    -------------------
    sudo apt install xrdp
    sudo apt install xfce4
    echo "xfce4-session" > ~/.xsession
    /etc/init.d/xrdp start
    /etc/init.d/xrdp status

    Remap keyboard
    --------------
    sudo vi /etc/default/keyboard
    replace "XKBOPTIONS="ctrl:nocaps"
    sudo dpkg-reconfigure keyboard-configuration

    Change default terminal
    -----------------------
    sudo update-alternatives --config -x-terminal-emulator 


    Update SSH key password
    -----------------------
    ssh-keygen -f ~/.ssh/id_rsa -p


    Vim setting
    -----------
    cp `/usr/share/vim/vimrc` ~/.vimrc

    Fix incorrect PATH when using XRDP
    ----------------------------------
    Add `. /etc/enviroment` to the top of `/etc/xrdp/startwm.sh`
```

# Synergy

* When completed, it should contain the synergy conf, and auto-start script at login screen for 
    * ubuntu
    * centos

# Source Insight

* Contain setting for Source Insight

# Git Config

## p4merge

* p4merge as `difftool` and `mergetool`
    * get the software from : https://www.perforce.com/downloads/helix#product-10
    * then, config the `.gitconfig` file to point to the applications, and setup the patho


## How to use non-bare repo (hack)

### Server

* Clone all the repository in the same dir
    * `git clone wpeh@gitserver:/pwc/bakery/pie`
    * `git clone wpeh@gitserver:/pwc/bakery/egg`
    * `git clone wpeh@gitserver:/pwc/bakery/flour`
    * `git clone wpeh@gitserver:/pwc/bakery/sugar`
* For each repo:
    * Fetch all the branches
        * `git fetch --all`
    * Ignore non-bare repo warning 
        * `git config receive.denyCurrentBranch ignore`
* For each push commit from Client:
    * Need to reset the HEAD to avoid dangling head 
        * `git reset --hard`

### Client

* Clone normally, but use ssh protocol instead of https
    * `git clone pwc@10.84.97.203:/home/pwc/work/pie`
* Rename the `Origin` variable to avoid confusion
    * `git remote rename origin berry`
* Get all the submodules 
    * `git submodule init`
    * `git submodule update`   <- please be patient
* To commit local changes
    * Commit and push all changes in external/egg, external/flour, external/sugar first
        * `git commit -am "Add flour, egg, sugar"` 
        * `git push berry branch`
    * Commit and push all changes + all submodule SHA-1 information
        * `git commit -am "Add my changes + SHA-1"`
        * `git push berry branch`


# Cygwin

* Contain the `minttyrc` color bash file + font file

# MSVS

~~~
* CTRL + F3        = Search in selection

* F4               = Definition
* F3               = Backward
* F2               = Forward

* F1               = Step Over
* SHIFT + F1       = Step Into
* ALT + F1         = Step Out

* CTRL + ,         = Find Symbols
* CTRL + ;         = Find Source File

* CTRL + F4        = Go to next location (Find All)
~~~

# MSVS Code

~~~

IDE
---
* CTRL + B         = Toggle Side Bar 
* CTRL + L         = Collapse all directory
* CTRL + P         = Keep in editor (preview)
* CTRL + SHIFT + O = Open folder

Navigation
----------
* CTRL + E/Y       = Scroll line up/down
* CTRL + -         = Go to previous
* CTRL + SHIFT + - = Go to forward

File
----
* CTRL + ;         = Find Source File
* Alt + W          = Close all open file



~~~

# Webtools

* Use ngrok for SSH

Setup server
~~~
sudo apt-get install ngrok-client ngrok-server
ngrok --proto=tcp 22
~~~

SSH client
```
You should use: [username]@0.tcp.ngrok.io -p3356
```
