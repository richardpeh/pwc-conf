#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Wake-On-LAN
#
# Copyright (C) 2002 by Micro Systems Marc Balmer
# Written by Marc Balmer, marc@msys.ch, http://www.msys.ch/
# This code is free software under the GPL

import struct
import socket

def WakeOnLan(ethernet_address):
    # Construct a six-byte hardware address
    addr_byte = ethernet_address.split(':')
    hw_addr = struct.pack('BBBBBB', int(addr_byte[0], 16),
                          int(addr_byte[1], 16),
                          int(addr_byte[2], 16),
                          int(addr_byte[3], 16),
                          int(addr_byte[4], 16),
                          int(addr_byte[5], 16))

    # Build the Wake-On-LAN "Magic Packet"...
    msg = '\xff' * 6 + hw_addr * 16

    # ...and send it to the broadcast address using UDP
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.sendto(msg, ('<broadcast>', 9))
    s.close()

#
if __name__ == "__main__":
    #WakeOnLan('00:10:18:8A:6E:89')   # sg2-satt20 LAN#1
    #WakeOnLan('BC:30:5B:D2:13:28')   # sg2-satt20 LAN#2
    #WakeOnLan('00:15:C5:A6:80:22')   # dell laptop
    #WakeOnLan('00:26:9E:B3:37:31')   # gateway laptop (dun support wol)
    #
    def wakeOberon():
        WakeOnLan('6c:f0:49:04:63:65')   # oberon xbmc (support wol)

    def wakeSGPU386477D010():
        WakeOnLan('1C:6F:65:D3:34:5E')   # desktop (support wol)

    def wakeOPENSTACK01():
        WakeOnLan('08:62:66:A1:9D:C1')   # openstack server

    options = ('Oberon XBMC', 'SGP-U386477D010', 'OPENSTACK01')
    callbacks = (wakeOberon, wakeSGPU386477D010, wakeOPENSTACK01)

    for offset, option in enumerate(options):
        print('%s: %s' % (offset, option))

    choice = int(raw_input('Your choice? '))
    callbacks[choice]()
