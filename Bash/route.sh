# route -n
# sudo /etc/init.d/networking restart
# sudo route del -net 0.0.0.0 gw 192.168.0.254 netmask 0.0.0.0 dev eno1

# Adding this conf at the top, making it the 'default'
sudo route add default gw 10.20.0.1 netmask 0.0.0.0
