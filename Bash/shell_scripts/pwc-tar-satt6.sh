#Create File Name with Date in it
FILE=hgst-satt6_`date +%d%b%y`.tar

#Print out the $FILE name
echo $FILE

#Combine the File Name with the Destination Path
FILE=/backup/$FILE 

echo $FILE

#Tar backup script:
#tar -cvpzf $FILE --directory=/ --exclude=mnt/* --exclude=backup/* --exclude=proc/* --exclude=tmp/* --exclude=workbench/* --exclude=media/* --exclude=dev/ /

tar -cvpzf $FILE --directory=/ --exclude=mnt/* --exclude=backup/* --exclude=proc/* --exclude=tmp/* --exclude=workbench/* --exclude=media/* --exclude=dev/ --exclude=/sys --exclude=boot/grub --exclude=etc/fstab --exclude=etc/sysconfig/network-scripts/ --exclude=etc/udev/rules.d/70-persistent-net.rules /

## compression .gz option:
##tar -cvpf $FILE --directory=/shareroot/trex/users/rp/ --exclude=tar_folder/* --exclude=TREX --exclude=jTiny .

echo Backup file saved to $FILE
